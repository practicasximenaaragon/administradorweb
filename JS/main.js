
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAPFy3q8D7vHFV5rbwTqtu5Dn4TJyNUW3M",
    authDomain: "proyectowebfinal-458d3.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-458d3-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-458d3",
    storageBucket: "proyectowebfinal-458d3.appspot.com",
    messagingSenderId: "529450401752",
    appId: "1:529450401752:web:d3d27f2f90790a9ca53cad"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// variables para manejo de la imagen

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas variables goblales
var numSerie =0;
var marca ="";
var modelo ="";
var descripcion="";
var urlImag ="";



//funciones
function leerInputs(){
  numSerie= document.getElementById('txtNumSerie').value;
  marca= document.getElementById('txtMarca').value;
  modelo= document.getElementById('txtModelo').value;
  descripcion= document.getElementById('txtDescripcion').value;
  urlImag = document.getElementById('txtUrl').value;

}
function mostrarMensaje(mensaje){
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent= mensaje;
  mensajeElement.style.display= 'block';
  setTimeout(()=>{
    mensajeElement.style.display ='none'
  },1000);
}

//agregar producto a la base de datos
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto(){
  alert("ingrese a add db");
  leerInputs();
  //validar
  if(numSerie==="" || marca==="" || modelo==="" || descripcion===""){
    mostrarMensaje("faltaron datos por capturar");
    return;
  }
  // --- function de firebase para agregar registro
  set(
    refS(db,'Automoviles/' + numSerie),

 {
  /*datos a guardar
  realizar json con los campos y datos de la tabla
  campo:valor
  */
 numSerie:numSerie,
 marca:marca,
 modelo:modelo,
 descripcion:descripcion,
 urlImag:urlImag
}
 ).then(()=>{

  alert("Se agrego con exito");

 }).catch ((error)=>{
  alert("Ocurrio un error");
 });

 Listarproductos();
 limpiarInputs();
 
}
// codifica el boton de buscar 
// agregar las siguientes funciones

function limpiarInputs(){
  document.getElementById('txtNumSerie').value = '';
  document.getElementById('txtModelo').value = '';
  document.getElementById('txtMarca').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';

}

function escribirInputs(){

  document.getElementById('txtModelo').value = modelo;
   document.getElementById('txtMarca').value = marca;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;

}

function buscarProducto() {
  let numSerie = document.getElementById('txtNumSerie').value.trim();
  if (numSerie=== "") {
    mostrarMensaje("No se ingreso Numero de Serie");
    return;
  }

  const dbref = refS(db);
  get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
    if (snapshot.exists()){
      numSerie = snapshot.val().numSerie
      marca = snapshot.val().marca;
      modelo = snapshot.val().modelo;
      descripcion = snapshot.val().descripcion;
      urlImag = snapshot.val().urlImag;
      escribirInputs();
    }else{
      limpiarInputs();
      mostrarMensaje ( "El carro con numero de serie" + numSerie + " NO existe.");

    }
  });
}
btnBuscar.addEventListener('click', buscarProducto);

Listarproductos()
function Listarproductos() {
  const dbref = refS(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';
  onValue(dbref, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      
  const childKey = childSnapshot.key;

  const data = childSnapshot.val();
  var fila = document.createElement('tr');

       
      var celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);

      var celdaNombre = document.createElement('td');
      celdaNombre.textContent  = data.marca;
      fila.appendChild(celdaNombre);

      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent  = data.modelo;
      fila.appendChild(celdaPrecio);

      var celdaCantidad = document.createElement('td');
      celdaCantidad.textContent  = data.descripcion;
      fila.appendChild(celdaCantidad);

      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.urlImag;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);
      tbody.appendChild(fila);
       
    });
  }, { onlyOnce: true });
}
//Se agrega la funcion actualizar
function actualizarAutomovil() {
  leerInputs();
  if (numSerie === "" || marca === "" || modelo === "" || descripcion === ""){
    mostrarMensaje("Favor de capturar toda la informacion.");
    return;
  }
  alert("actualizar");
  update(refS(db, 'Automoviles/' + numSerie), {
    numSerie:numSerie,
    marca: marca,
    modelo: modelo,
    descripcion: descripcion,
    urlImag: urlImag
  }).then(() => {
    mostrarMensaje("Se actualizo con exito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) =>{
    mostrarMensaje("Ocurrio un error: " + error);
  });
 }

function eliminarAutomovil(){
  let numSerie= document.getElementById('txtNumSerie').value.trim();
  if (numSerie === ""){
    mostrarMensaje("No se ingreso un numero de serie valido.");
    return;
  }
  const dbref = refS(db);
  get (child(dbref,'Automoviles/' + numSerie)).then((snapshot) => {

    if (snapshot.exists()){
      remove(refS(db, 'Automoviles/'+ numSerie))
      .then(() => {
        mostrarMensaje("Automovil eliminado con exito.");
        limpiarInputs();
        Listarproductos();
      })
      .catch((error) => {
        mostrarMensaje("Ocurrio un error al eliminar el automovil: " + error);

      });
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con ID " + numSerie + " no existe.");

    }
  });
  Listarproductos();
}
uploadButton.addEventListener('click', (event) => {
  event.preventDefault();
  const file = imageInput.files[0];

  if (file) {
    const storageRef = ref(storage, file.name);
    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on('state_changed', (snapshot) => {
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';

    }, (error) => {
      console.error(error);
    }, () => {
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        txtUrlInput.value = downloadURL;
        setTimeout(() => {
          progressDiv.textContent = '';
        }, 500);
      }).catch((error) => {
        console.error(error);
      });
    });
  }
});

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);

