
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAPFy3q8D7vHFV5rbwTqtu5Dn4TJyNUW3M",
    authDomain: "proyectowebfinal-458d3.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-458d3-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-458d3",
    storageBucket: "proyectowebfinal-458d3.appspot.com",
    messagingSenderId: "529450401752",
    appId: "1:529450401752:web:d3d27f2f90790a9ca53cad"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// variables para manejo de la imagen

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas variables goblales
var numSerie =0;
var marca ="";
var modelo ="";
var descripcion="";
var urlImag ="";




// listar productos


function Listarproductos() {
    const titulo= document.getElementById("titulo");
    const  section = document.getElementById('contenedorflexy')
    titulo.innerHTML= marca; 
    
    
  const dbRef = refS(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
 
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;

          const data = childSnapshot.val();
         
          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200' height='200' > "
                     + "<h1 id='marca'  class='title'>" + data.marca+ ":" + data.modelo +"</h1> "
                     + "<p  id='descripcion' class ='anta-regurar'>" + data.descripcion 
                     +  "</p> <button> Mas Informacion </button> </div>"  ;           


      });
  }, { onlyOnce: true });
}

Listarproductos("NISSAN");